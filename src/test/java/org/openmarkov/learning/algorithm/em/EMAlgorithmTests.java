/**
 * 
 */
package org.openmarkov.learning.algorithm.em;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.exception.NormalizeNullVectorException;
import org.openmarkov.core.exception.ProbNodeNotFoundException;
import org.openmarkov.core.io.database.CaseDatabase;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.PotentialRole;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.UniformPotential;

/**
 * @author Iñigo
 *
 * Tests for the Expectation-Maximization algorithm 
 */
public class EMAlgorithmTests
{

    private EMAlgorithm algorithm;    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp ()
        throws Exception
    {
        ProbNet probNet = new ProbNet();

        Variable C = new Variable("C", "C0", "C1", "C2"); 
        Variable X1 = new Variable("X1", "X10", "X11", "X12"); 
        Variable X2 = new Variable("X2", "X20", "X21", "X22"); 
        Variable X3 = new Variable("X3", "X30", "X31", "X32");
        
        probNet.addProbNode (C, NodeType.CHANCE);
        probNet.addProbNode (X1, NodeType.CHANCE);
        probNet.addProbNode (X2, NodeType.CHANCE);
        probNet.addProbNode (X3, NodeType.CHANCE);
        
        probNet.addLink (C, X1, true);
        probNet.addLink (C, X2, true);
        probNet.addLink (C, X3, true);
        

        // Build a dummy probNet to pass the variable list to the CaseDatabase constructor
        ProbNet probNetDB = new ProbNet();
        probNetDB.addProbNode (X1, NodeType.CHANCE);
        probNetDB.addProbNode (X2, NodeType.CHANCE);
        probNetDB.addProbNode (X3, NodeType.CHANCE);
        
        TablePotential x1Potential = new TablePotential (new UniformPotential (PotentialRole.CONDITIONAL_PROBABILITY, X1, C));
        TablePotential x2Potential = new TablePotential (new UniformPotential (PotentialRole.CONDITIONAL_PROBABILITY, X2, C));
        TablePotential x3Potential = new TablePotential (new UniformPotential (PotentialRole.CONDITIONAL_PROBABILITY, X3, C));
        TablePotential cPotential  = new TablePotential (new UniformPotential (PotentialRole.CONDITIONAL_PROBABILITY, C));
        
        probNet.addPotential (x1Potential);
        probNet.addPotential (x2Potential);
        probNet.addPotential (x3Potential);
        probNet.addPotential (cPotential);
        
        
        int cases [][] ={{0, 1, 0}, {1, 0, 0}, {1, 1, 1}};
        List<Variable> variables = new ArrayList<> ();
        variables.add (X1);
        variables.add (X2);
        variables.add (X3);
        
        CaseDatabase caseDatabase = new CaseDatabase (variables, cases);
        
        algorithm = new EMAlgorithm (probNet, caseDatabase, 0.0);
        
    }

    @Test
    public void test () throws NormalizeNullVectorException, ProbNodeNotFoundException
    {
        ProbNet learnedNet = algorithm.parametricLearning ();
        Assert.assertNotNull (learnedNet);
        Assert.assertEquals (4, learnedNet.getNumNodes ());
    }
}
